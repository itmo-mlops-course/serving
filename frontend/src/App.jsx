import ImageProcessor from "./components/imageProcessor.jsx";
import {Layout} from "antd";
import {Content, Header} from "antd/es/layout/layout.js";
import Title from "antd/es/typography/Title.js";

export default function App() {
    return (
        <Layout>
            <Header
                style={{
                    display: 'flex',
                    alignItems: 'center',
                    padding: '0 20px',
                }}
            >
                <Title level={4} style={{color: 'white', margin: 0}}>MLOps Course - KELONMYOSA</Title>
            </Header>
            <Content
                style={{
                    padding: '0 20px',
                    height: 'calc(100vh - 64px)',
                }}
            >
                <ImageProcessor/>
            </Content>
        </Layout>
    )
}
