import React, {useState} from 'react';
import {Upload, Button, Image, Spin, message, Space, Row, Col} from 'antd';
import {InboxOutlined, LoadingOutlined, RollbackOutlined} from '@ant-design/icons';
import axios from 'axios';
import Title from "antd/es/typography/Title.js";

const ImageProcessor = () => {
    const apiUrl = import.meta.env.VITE_API_URL

    const [image, setImage] = useState(null);
    const [processedImage, setProcessedImage] = useState(null);
    const [loading, setLoading] = useState(false);
    const [filename, setFilename] = useState(null);

    const beforeUpload = (file) => {
        const isImage = file.type.startsWith('image/');
        if (!isImage) {
            message.error('You can only upload image files!');
        }
        return isImage;
    };

    const handleUpload = ({file}) => {
        setImage(file);
        setFilename(file.name);
    };

    const handleSubmit = async () => {
        if (!image) {
            message.error('Please upload an image first!');
            return;
        }

        const formData = new FormData();
        formData.append('file', image);

        setLoading(true);
        try {
            const response = await axios.post(`${apiUrl}/yolo/predict`, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                },
                responseType: 'blob',
            });

            const blob = new Blob([response.data], {type: response.data.type});
            const imageUrl = URL.createObjectURL(blob);

            setProcessedImage(imageUrl);
            setLoading(false);
        } catch (error) {
            console.error('Error uploading the image:', error);
            setLoading(false);
            message.error('Error uploading the image!');
        }
    };

    const handleTryAgain = () => {
        setImage(null);
        setProcessedImage(null);
        setFilename(null);
    };

    return (
        <div style={{textAlign: 'center', padding: '20px 0'}}>
            {!processedImage ? (
                <div>
                    <Title level={3} style={{textAlign: 'start'}}>Road sign recognition</Title>
                    <Upload.Dragger
                        beforeUpload={beforeUpload}
                        customRequest={handleUpload}
                        showUploadList={false}
                        multiple={false}
                    >
                        <p className="ant-upload-drag-icon">
                            <InboxOutlined/>
                        </p>
                        <p className="ant-upload-text">{filename ? filename : 'Click or drag file to this area to upload'}</p>
                    </Upload.Dragger>
                    <Button
                        type="primary"
                        onClick={handleSubmit}
                        disabled={!image}
                        style={{marginTop: '20px'}}
                        size='large'
                    >
                        {loading ? <Spin indicator={<LoadingOutlined style={{fontSize: 24}} spin/>}/> : 'Submit'}
                    </Button>
                </div>
            ) : (
                <div style={{width: '100%'}}>
                    <Row justify='center'>
                        <Col xs={24} md={16}>
                            <Space
                                direction="vertical"
                                size="middle"
                                style={{
                                    alignItems: 'start',
                                }}
                            >
                                <Button type='primary' icon={<RollbackOutlined/>} onClick={handleTryAgain}>
                                    Try Again
                                </Button>
                                <Image
                                    width='100%'
                                    src={processedImage}
                                    alt="Processed"
                                />
                            </Space>
                        </Col>
                    </Row>
                </div>
            )}
        </div>
    );
};

export default ImageProcessor;
