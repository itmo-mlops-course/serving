import io

from PIL import Image

from src.models.yolo import device, model


def predict_img(img_path: str) -> bytes:
    result = model(img_path, conf=0.5, imgsz=1280, device=device)
    img_array = result[0].plot()
    img = Image.fromarray(img_array)

    with io.BytesIO() as buf:
        img.save(buf, format="JPEG")
        img_bytes = buf.getvalue()

    return img_bytes
