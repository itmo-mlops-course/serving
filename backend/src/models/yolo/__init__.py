import logging

import mlflow
import torch
from ultralytics import YOLO

from src.config import settings

logging.getLogger("ultralytics").setLevel(logging.ERROR)

mlflow.set_tracking_uri(settings.MLFLOW_TRACKING_URI)
mlflow.artifacts.download_artifacts(artifact_uri=settings.LATEST_YOLO_URI, dst_path="src/models")

model = YOLO(f'src/models/{settings.LATEST_YOLO_URI.split("/")[-1]}')
device = "cuda:0" if torch.cuda.is_available() else "cpu"
model.to(device)
