import os
from tempfile import NamedTemporaryFile

from fastapi import APIRouter, File, HTTPException, Response, UploadFile, status

from src.models.yolo.predict import predict_img

router = APIRouter(
    prefix="/yolo",
    tags=["YOLOv8 Model"],
)


@router.post("/predict", response_class=Response)
async def get_img_predicted(file: UploadFile = File(...)):  # noqa: B008
    file_ext = os.path.splitext(file.filename)[1]
    if file_ext not in [".bmp", ".dng", ".jpg", ".jpeg", ".mpo", ".png", ".tif", ".tiff", ".webp", ".pfm"]:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST, detail="There was an error with the file extension"
        )

    try:
        with NamedTemporaryFile("wb", suffix=file_ext, delete=False) as temp:
            try:
                contents = await file.read()
                temp.write(contents)
            except Exception:
                raise HTTPException(  # noqa: B904
                    status_code=status.HTTP_400_BAD_REQUEST, detail="There was an error uploading the file"
                )
            finally:
                await file.close()

        res = predict_img(temp.name)
    except Exception:
        raise HTTPException(  # noqa: B904
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR, detail="There was an error processing the file"
        )
    finally:
        os.remove(temp.name)

    headers = {"Content-Disposition": 'inline; filename="processed.jpeg"'}
    return Response(res, headers=headers, media_type="image/jpeg")
