from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file="../.env", env_file_encoding="utf-8", extra="ignore")

    MLFLOW_TRACKING_URI: str
    LATEST_YOLO_URI: str


settings = Settings()
